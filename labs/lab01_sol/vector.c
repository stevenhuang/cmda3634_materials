#include <stdio.h>
#include <math.h>

typedef struct Vector_tag {
    float data[3];
} Vector;


int initialize(Vector* u){

    for(int i = 0; i < 3; ++i){
        u->data[i] = 0.0;
    }

    return 0;
}

int inner_product(Vector* u, Vector* v, float* sum){

    *sum = 0.0;

    for (int i = 0; i < 3; ++i){
        *sum += u->data[i]*v->data[i];
    }

    return 0;
}

float norm(Vector* u){

    // return sqrt(inner_product(u, u));
    float ip;
    inner_product(u, u, &ip);
    return sqrt(ip);
}

int normalize(Vector* u){

    float len = norm(u);

    if (len < 1e-6) return 1;

    for(int i = 0; i < 3; ++i){
        u->data[i] /= len;
    }

    return 0;
}

int axpy(float a, Vector* u, Vector* v, Vector* w){

    for (int i = 0; i < 3; ++i){
        w->data[i] = a*u->data[i] + v->data[i];
    }

    return 0;
}

void print(Vector* u){

    int n = 3;
    printf("[");
    for(int i = 0; i < n-1; ++i){
        printf("%f, ", u->data[i]);
    }
    printf("%f]", u->data[n-1]);
}

int main(){

    // Two input 3-vectors
    Vector u;
    Vector v;

    // An output vector that we will re-use

    // Initialize the input vectors
    u.data[0] = 3.0;
    u.data[1] = 1.6;
    u.data[2] = 7.3;

    v.data[0] = 0.01;
    v.data[1] = 3.45;
    v.data[2] = 1414.14;

    printf("Initial setup:\n");

    printf("    u = ");
    print(&u);
    printf("\n");
    printf("    v = ");
    print(&v);
    printf("\n");
    printf("\n");

    float m = norm(&u);
    printf("i) m = %f\n", m);
    printf("\n");

    Vector w1;
    float alpha = 0.53;
    axpy(alpha, &u, &v, &w1);
    printf("ii) w1 = ");
    print(&w1);
    printf("\n");
    printf("\n");

    Vector w2;
    float beta = 0.77;
    axpy(beta, &v, &v, &w2);
    printf("iii) w2 = ");
    print(&w2);
    printf("\n");
    printf("\n");

    float a;
    inner_product(&u, &v, &a);
    printf("iv) a = %f\n", a);
    printf("\n");

    normalize(&u);
    printf("v) u_hat = ");
    print(&u);
    printf("\n");
    printf("\n");

    normalize(&v);
    printf("vi) v_hat = ");
    print(&v);
    printf("\n");
    printf("\n");
 
    return 0;


}