#include <stdio.h>
#include <math.h>

#include "vector.h"


int main(){

    // Two input 3-vectors
    Vector u;
    Vector v;

    // An output vector that we will re-use

    // Initialize the input vectors

    allocate(&u, 3);
    allocate(&v, 3);

    u.data[0] = 3.0;
    u.data[1] = 1.6;
    u.data[2] = 7.3;

    v.data[0] = 0.01;
    v.data[1] = 3.45;
    v.data[2] = 1414.14;

    printf("Initial setup:\n");

    printf("    u = ");
    print(&u);
    printf("\n");
    printf("    v = ");
    print(&v);
    printf("\n");
    printf("\n");

    float m = norm(&u);
    printf("i) m = %f\n", m);
    printf("\n");

    Vector w1;
    allocate(&w1, 3);
    initialize(&w1);
    float alpha = 0.53;
    axpy(alpha, &u, &v, &w1);
    printf("ii) w1 = ");
    print(&w1);
    printf("\n");
    printf("\n");

    Vector w2;
    allocate(&w2, 3);
    initialize(&w2);
    float beta = 0.77;
    axpy(beta, &u, &v, &w2);
    printf("iii) w2 = ");
    print(&w2);
    printf("\n");
    printf("\n");

    float a;
    inner_product(&u, &v, &a);
    printf("iv) a = %f\n", a);
    printf("\n");

    normalize(&u);
    printf("v) u_hat = ");
    print(&u);
    printf("\n");
    printf("\n");

    normalize(&v);
    printf("vi) v_hat = ");
    print(&v);
    printf("\n");
    printf("\n");
 
    deallocate(&u);
    deallocate(&v);
    deallocate(&w1);
    deallocate(&w2);



    return 0;


}
