/*
* Authors:
*   Russell J. Hewett (rhewett@vt.edu)
*
* Not licensed for external distribution.
*/

#ifndef __SIMPLE_ARRAY_IO_H__
#define __SIMPLE_ARRAY_IO_H__

int read_simple_array_format(char* filename, float** data, int* n_r, int* n_c, int* n_channels);

#endif