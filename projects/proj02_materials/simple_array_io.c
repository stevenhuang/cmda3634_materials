#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "simple_array_io.h"


/*
* Read in an array with the provided simple data specification.
*
* WARNING:  This function allocates the data array.
*
* This function can be called with the following example:

float* my_data;
int rows, cols, channels;
read_simple_array_format("path/to/file.arr", &my_data, &rows, &cols, &channels);

* Afterward, rows, cols, and channels contain the correct sizes and my_data
* points to the array.
*
* Arguments:
*   filename: string, path to the file to read
*   data: pointer to the floating point data we will read into
*   n_r: pointer to where to store number of rows in the array
*   n_c: pointer to where to store number of columns in the array
*   n_channels: pointer to where to store number of color channels in the array (should always be 4)
*
* Returns:
*   error code, 0 for success, 1 for failure
*/
int read_simple_array_format(char* filename, float** data, int* n_r, int* n_c, int* n_channels){

    FILE* f;
    unsigned char val;
    int idx;

    // Open the file
    f = fopen(filename, "rb");
    if (f == NULL) {
        fprintf(stderr, "Error opening file %s for write.\n", filename);
        return 1;
    }

    int n_read;

    // Read the header
    n_read = fread(n_r, sizeof(unsigned int), 1, f);
    if (n_read != 1){
        fprintf(stderr, "Error reading nr from file %s: %d of 1 data read.\n", filename, n_read);
        return 1;
    }

    n_read = fread(n_c, sizeof(unsigned int), 1, f);
    if (n_read != 1){
        fprintf(stderr, "Error reading nc from file %s: %d of 1 data read.\n", filename, n_read);
        return 1;
    }

    // Cannot just read an int, need a ptr to it.
    n_read = fread(n_channels, sizeof(unsigned int), 1, f);
    if (n_read != 1){
        fprintf(stderr, "Error reading channels from file %s: %d of 1 data read.\n", filename, n_read);
        return 1;
    }
    if(*n_channels != 4){
        fprintf(stderr, "Error reading channels from file %s: %d is not 4.\n", filename, *n_channels);
        return 1;
    }

    // Allocate space for the data
    unsigned int n_data = (*n_r) * (*n_c) * (*n_channels);
    *data = malloc(n_data*sizeof(float));

    if (data == NULL){
        fprintf(stderr, "Error allocating data array.\n");
        return 1;
    }

    // Read the data
    n_read = fread(*data, sizeof(float), n_data, f);
    if (n_read != n_data){
        fprintf(stderr, "Error reading data from file %s: %d of %d data read.\n", filename, n_read, n_data);
        return 1;
    }

    fclose(f);

    return 0;
}
