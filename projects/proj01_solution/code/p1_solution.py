import numpy as np

def random_init(n_r, n_c, p, fully_random_color=True):
    """
    Creates the n_r X n_c X 4 initial conditions for RGB Life.

    Parameters
    ----------
    n_r: integer
       Number of rows in the simulation grid.
    n_c: integer
       Number of columns in the simulation grid.
    p: float
       Probability that a cell starts out alive.
    fully_random_color: Boolean, optional.
       It is convenient for colors to start out with some contrast.  If True
       Colors are fully random. If False, colors are purely R, G, or B.

    Returns
    -------
    g_init: numpy.ndarray
        The initial state.

    """

    # Obtain an empty array of the correct size.  All cells start out dead.
    g_init = np.zeros((n_r, n_c, 4))

    # Visit each cell
    for i in range(n_r):
        for j in range(n_c):

            # Determine if the cell is alive.
            alive = np.random.rand(1) < p

            # If it is alive, mark it so.
            if alive:
                g_init[i,j,3] = 1

                # If we are to fully randomize the color, do so,
                # otherwise choose a single channel to turn on.
                if fully_random_color:
                    g_init[i,j,0:3] = np.random.rand(3)
                else:
                    c = np.random.choice([0,1,2])
                    g_init[i,j,c] = 1

    return g_init


def hash_grid(g):
    """
    Evaluates a hash function on a grid.

    Parameters
    ----------
    g: numpy.ndarray
        The initial generation to hash.

    Returns
    -------
    h: int
        The hash of g.

    """

    n_r, n_c, n_channels = g.shape

    h = 0
    for i in range(n_r):
        for j in range(n_c):
            h += i + j*g[i,j,3]*(int(g[i,j,0]*255) + 10*int(g[i,j,1]*255) + 100*int(g[i,j,2]*255))

    return h

def evolve_onestep(g_curr, g_next):
    """
    Computes a single evolution of a generation of cells following the rules of RGB Life.

    Parameters
    ----------
    g_curr: numpy.ndarray
        The current generation.
    g_next: numpy.ndarray
        The evolved generation, updated in-place.

    """

    # Get the shape of the input
    n_r, n_c, n_channels = g_curr.shape

    # Loop over each grid point
    for i in range(n_r):
        # Get the up and down indices
        i_U = (i-1)%n_r
        i_D = (i+1)%n_r

        for j in range(n_c):
            # Get the left and right indices
            j_L = (j-1)%n_c
            j_R = (j+1)%n_c

            # Add up all properties of the neighboring cells.  For this to work we
            # assume that all dead cells have color (0,0,0).
            sum_neighbors = g_curr[i_U, j_L, :] + g_curr[i_U, j, :] + g_curr[i_U, j_R, :] + \
                            g_curr[i,   j_L, :]                     + g_curr[i  , j_R, :] + \
                            g_curr[i_D, j_L, :] + g_curr[i_D, j, :] + g_curr[i_D, j_R, :]

            living_neighbors = sum_neighbors[3]

            # Implement the rules of RGB Life.
            if (g_curr[i,j,3] == 0 and living_neighbors == 3):
                g_next[i,j,:] = sum_neighbors/living_neighbors
            elif (g_curr[i,j,3] == 1 and living_neighbors in (2,3)):
                g_next[i,j,:] = g_curr[i,j,:]
            else:
                g_next[i,j,:] = 0

def evolve_simulation(g_init, n_generations):
    """
    Computes multiple evolutions of a generation of cells following the rules of RGB Life.

    Parameters
    ----------
    g_init: numpy.ndarray
        The starting generation.
    n_generations: int
        Number of generations to run.

    Returns
    -------
    G: list
        List of n_generations+1 generations, including the initial generation.

    """

    n_r, n_c, n_channels = g_init.shape

    # Initialize solution and storage
    G = list()
    g_curr = g_init.copy()
    g_next = np.zeros((n_r, n_c, n_channels))

    # Perform the simulation
    for k in range(n_generations):

        # Copy the current state
        G.append(g_curr.copy())

        # Perform the evolution
        evolve_onestep(g_curr, g_next)

        # Swap all of the array references
        g_curr, g_next = g_next, g_curr

    # Be sure to capture the last solution state
    G.append(g_curr.copy())

    return G
