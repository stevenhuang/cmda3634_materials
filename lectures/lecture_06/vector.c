#include <stdio.h>

int main(){

    // Two input 3-vectors
    float u_x, u_y, u_z;
    float v_x, v_y, v_z;

    // An output vector that we will re-use
    float w_x, w_y, w_z;

    // Initialize the input vectors
    u_x = 1.0;
    u_y = 1.0;
    u_z = 1.0;

    v_x = 1.1;
    v_y = 2.2;
    v_z = 3.3;

    printf("Initial setup:\n");
    printf("    ux, uy, uz = %f, %f, %f\n", u_x, u_y, u_z);
    printf("    vx, vy, vz = %f, %f, %f\n", v_x, v_y, v_z);
    printf("\n");


    // Compute the inner product of u and v
    // The inner product is the same as the dot product
    float ip_uv;


    printf("Inner product:\n");
    printf("    <u, v> = %f\n", ip_uv);
    printf("\n");


    // Compute the norm of u
    float norm_u;


    printf("Norm:\n");
    printf("    ||u|| = sqrt{<u, u>} = %f\n", norm_u);
    printf("\n");

    // Compute the "axpy" operation (constant times a vector plus a vector.)
    // See http://www.netlib.org/lapack/explore-html/d8/daf/saxpy_8f.html
    // for more details.

    float alpha = 0.5;


    printf("AXPY: w = a*u + v\n");
    printf("    wx, wy, wz = %f, %f, %f\n", w_x, w_y, w_z);
    printf("\n");
 
    return 0;

}
