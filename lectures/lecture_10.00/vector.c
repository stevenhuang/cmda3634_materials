#include <stdio.h>
#include <math.h>

typedef struct Vector_tag {
    float data[3];
} Vector;

int initialize(Vector* u){

    for (int i = 0; i < 3; ++i){
        u->data[i] = 0.0;
    }

    return 0;
}

int inner_product(Vector* u, Vector* v, float* sum){

    *sum = 0.0;

    for (int i = 0; i < 3; ++i){
        *sum += u->data[i]*v->data[i];
    }

    return 0;
}

float norm(Vector* u){

    float ip;
    inner_product(u, u, &ip);
    return sqrt(ip);
}

int normalize(Vector* u){

    float len = norm(u);

    if (len < 1e-7)
        return 1;

    for(int i = 0; i < 3; ++i){
        u->data[i] /= len;
    }

    return 0;
}


int axpy(float a, Vector* u, Vector* v, Vector* w){
    
    for (int i = 0; i < 3; ++i){
        w->data[i] = a*u->data[i] + v->data[i];
    }

    return 0;
}

void print_Vector(Vector* u){

    int n = 3;
    printf("[");
    for(int i = 0; i < n-1; ++i){
        printf("%f, ", u->data[i]);
    }
    printf("%f]", u->data[n-1]);
}

int main(){

    // Two input 3-vectors
    Vector u, v, w;

    initialize(&u);
    initialize(&v);
    initialize(&w);

    // Initialize the input vectors
    u.data[0] = 1.0;
    u.data[1] = 1.0;
    u.data[2] = 1.0;

    v.data[0] = 1.1;
    v.data[1] = 2.2;
    v.data[2] = 3.3;

    printf("Initial setup:\n");

    printf("    u = ");
    print_Vector(&u);
    printf("\n");

    // normalize(u);
    normalize(&u);

    printf("    u = ");
    print_Vector(&u);
    printf("\n");

    printf("    v = ");
    print_Vector(&v);
    printf("\n");
    printf("\n");


    // Compute the inner product of u and v
    // The inner product is the same as the dot product
    float ip_uv;

    // ip_uv = inner_product(u, v);
    inner_product(&u, &v, &ip_uv);

    printf("Inner product:\n");
    printf("    <u, v> = %f\n", ip_uv);
    printf("\n");


    // Compute the norm of u
    float norm_u;

    norm_u = norm(&u);

    printf("Norm:\n");
    printf("    ||u|| = sqrt{<u, u>} = %f\n", norm_u);
    printf("\n");

    // Compute the "axpy" operation (constant times a vector plus a vector.)
    // See http://www.netlib.org/lapack/explore-html/d8/daf/saxpy_8f.html
    // for more details.

    float alpha = 0.5;

    axpy(alpha, &u, &v, &w);

    printf("AXPY: w = a*u + v\n");
    printf("    w = ");
    print_Vector(&w);
    printf("\n");
    printf("\n");
 
    return 0;


}