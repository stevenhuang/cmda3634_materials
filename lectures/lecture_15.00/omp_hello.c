#include <omp.h>
#include <stdio.h>

int main(int argc, char **argv) {

    omp_set_num_threads(8);

#pragma omp parallel
    {
        int thread = omp_get_thread_num();
        int Nthreads = omp_get_num_threads();

        printf("hello from thread %d of %d\n",thread,Nthreads);
    }

    return 0;
}
