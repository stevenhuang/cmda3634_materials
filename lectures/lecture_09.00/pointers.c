#include <stdio.h>

int main()
{
    int n_trials = 5;

//  float trial1, trial2, trial3, ...

    float temps[n_trials];

    temps[0] = 70.0;
    temps[1] = 84.5;
    temps[2] = 32.8;
    temps[3] = 56.1;
    temps[4] = 99.9;

    float average_temp = 0.0;

    for(int i=0; i < n_trials; i++){
        average_temp += temps[i];
    }

    average_temp /= (float)n_trials;

    printf("average temp = %f\n", average_temp);

    return 0;

}

