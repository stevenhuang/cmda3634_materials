#include <stdio.h>

float foo(){
	if (1) return bar();
	else return 5.0;
}

float bar(){
	if (1) return 7.0;
	else return foo();
}

int main(){
	printf("%f", foo());
}
