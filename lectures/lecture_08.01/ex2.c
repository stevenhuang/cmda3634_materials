#include <stdio.h>

float bar(){
	if (1) return 7.0;
	else return foo();
}

float foo(){
	if (1) return bar();
	else return 5.0;
}

int main(){
	printf("%f", foo());
}
