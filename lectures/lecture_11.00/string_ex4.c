#include <stdio.h>
int main(){

    // Create an array big enough to store what we need
    char buffer[50];

    char* experiment = "timing";
    int n = 40;
    int trial = 3;

    // Create a custom file name based on the data
    int n_chars;
    n_chars = sprintf(buffer, "exp_%s_%d_%d.txt", experiment, n, trial);

    printf("Created string\n    '%s'\nwhich has %d characters.", buffer, n_chars);

}